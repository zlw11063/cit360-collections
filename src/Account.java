// Eh, just import the whole thing instead of individual parts
import java.util.*;
import java.io.*;

public class Account {
    // Our four required fields
    private String accountNumber;
    private String accountType;
    private String givenName;
    private String familyName;

    // Constructor
    Account(String _accountNumber, String _accountType, String _givenName, String _familyName) {
        accountNumber = _accountNumber;
        accountType = _accountType;
        givenName = _givenName;
        familyName = _familyName;
    }

    @Override
    public String toString() {
        return "AN=" + accountNumber + " AT=" + accountType + " GN=" + givenName + " FN=" + familyName;
    }

    public static void main(String[] args) {
        // File Name
        String csvFile = "rawData.csv";
        // This will be used to store each line as we process them
        String thisLine = null;
        // Create an ArrayList to store all Account objects
        ArrayList<Account> accountArray = new ArrayList<>();
        // Create the BufferedReader, since it is out of scope within the try/catch
        BufferedReader br = null;
        // Register the file
        File file = new File("rawData.csv");
        // Make the TreeMap (accountType, Account object)
        TreeMap<String, Account> tMap = new TreeMap<>();

        try {
            try {
                // Build me a BufferedReader for our file
                br = new BufferedReader(new FileReader(csvFile));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            // Until we get to the bottom of the file...
            while ((thisLine = br.readLine()) != null) {

                // Print the current line to the console
                System.out.println(thisLine);

                // Split the line into fields (or each cell)
                String fields[] = thisLine.split(",");

                // Create new Account object for this line and add it to our ArrayList
                Account newAccount = new Account(fields[0], fields[1], fields[2], fields[3]);
                accountArray.add(newAccount);

                // Check if our TreeMap already contains this account
                if (!tMap.containsKey(fields[0])) {
                    // If it is not the first line (contains "account"), then add the account to the TreeMap
                    if (!fields[0].contains("account")) {
                        tMap.put(fields[0], newAccount);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Print the ArrayList
        System.out.println(accountArray);
        // Give me the TreeMap as well
        System.out.println(tMap.entrySet());
        // Give me a string of each Account in the TreeMap
        for(Map.Entry<String, Account> entry : tMap.entrySet()) {
            System.out.println(entry.getValue().toString());
        }
    }
}
